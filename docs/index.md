Bienvenue sur ma page FabLab!  Sur cette page vous pouvez suivre mon travail pour le cours FabZero

## Qui suis-je?
Hello moi c'est Emma :)

![me](./images/me.webp)

Je suis en 3ème année de mon bachelier en Bioingénieur à l'ULB.  

Je vais poursuivre mes études en Océanographie à Copenhague (croisons les doigts).  Le changement climatique et les impacts des humains sur l'environement ont toujours été un sujet important pour moi et je suis très interessée par les mers et les oceans qui nous entourent, principalement les phénomènes physiques des fonds marins et l'impact que les oceans ont sur notre climat.

En plus de vouloir travailler sur les océans, j'en ai fait un hobby. J'ai appris à faire de la voile sur les lacs belges (les vagues sont énormes sur les lacs belges c'est bien connu...) et cet été je pars faire un tour en **mer**  autour de l'Irlande!! J'espère plus tard pouvoir continuer à poursuivre cette passion :)
Comme tout le monde, j'adore écouter de la musique et surtout je suis une giga fan de [Taylor Swift](https://fieldstonnews.com/home/2021/01/taylor-swift-and-the-misogyny-within-the-music-industry/) (si t'es pas une swiftie es-tu vraiment heureuse.x?)
J'ai un amour passionné pour mon chat, légèrement obèse, je regarde les mêmes séries en boucle, j'adore les ramens et je passe beaucoup trop de temps sur TikTok. Je râle pour des choses inutiles, je rigole trop souvent aux blagues pas drôles et je sais parler islandais.


