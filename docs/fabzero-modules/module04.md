# 4. Outil Fab Lab sélectionné: Découpeuse laser

Cette semaine nous avons pu choisir un outil du FabLab à tester en groupe puis individuellement. J'ai décidé de m'initier à la découpeuse laser. Le premier cours est une introduction à l'outil et la semaine d'après je pourrais travailler sur une création personnelle.

## 4.1 Notes avant le cours

* Format du fichier pour la découpeuse ```.svg``` c'est-à-dire un fichier vectoriel.
* La modélisation se fait sur le logiciel Inkscape.
* Pour se familiariser avec Inkscape [voici le tutorial](https://inkscape.org/learn/tutorials/) que j'ai suivi.
* Le FabLab possède 3 types de découpeuses: 
  1. Epilog Fusion Pro 32
  2. Lasersaur
  3. Full Spectrum Muse

  On peut retrouver les manuels d'utilisation [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md).
* On peut réaliser des découpes, des gravures vectorielles ainsi que des gravures matricielles. Les gravures vectorielles suivent la même technique que les découpe mais avec une puissance plus faible, c'est-à-dire que le laser ne va pas traverser l'entièreté du matériel lors de son passage. Pour les gravures matricielles, celles-ci consistent en du dessin sur la surface du matériel.
* Il faut faire attention au choix du matériel car les gravures peuvent entrainer des fumées nocives,... 
* Les deux paramètres avec lesquels on peut jouer sont  **la puissance** et **la vitesse**. Ces paramètres peuvent varier en fonction du matériel. Pour plus d'information le FabLab a créé une [feuille récapitulative](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf) pour éviter les problèmes lors de la découpe.
* La découpeuse laser est un outil dangereux et il faut donc bien prendre en compte les [consignes de sécurité](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md) avant l'utilisation de l'outil.

## 4.2 Lors du cours 

Pendant le cours nous avons dû faire un test de sécurité avant de pouvoir utiliser les différentes machines. Ensuite, nous avons eu une petite présentation des différentes machines et des outils informatiques dont nous avions besoin.  Les informations importantes sur l'utilisation des 3 découpeuses sont réunies [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut)

Lors de la séance j'ai découvert le site [boxes.py](https://www.festi.info/boxes.py/) qui donne accès à des modèles de boîtes que l'on peut ensuite paramétrer comme on le désire. Le fichier peut ensuite être téléchargé en ```.svg``` et le modèle de boîte peut ensuite être découpé.

Nous avons vu 2 manières d'utiliser la découpeuse: 
* Découper 
* Graver

Lors de ce cours d'introduction nous avons dû tester ces deux techniques avec une des 3 machines. J'ai travaillé avec Louis sur la Laseraur. Pour commencer nous avons calibré la machine ain qu'lle ne découpe pas en dehors du matériel et que le laser n'endommage pas la machine. **Attention** il faut bien calibrer la machine avant chaque passage du laser.

Pour que la machine reconnaisse qu'on veut soit graver soit découper, il faut changer sa puissance et sa vitesse. Pour graver, il faudra plutôt diminuer la puissance du laser et augmenter sa vitesse. Pour graver nous avons utilisé une puissance de 50 et une vitesse de 200. Pour découper, nous utiliserons des proportions inverses, nous avons donc utilisé une puissance de 150 et une vitesse de 75. Nous avons dû repasser pour bien découper notre pièce.

Pour avoir une gravure et une découpe idéale, il aurait fallu faire des tests de puissance et de vitesse au préalable. Le fabLab a déjà  fait des tests pour un grand nombre de matériaux à différentes puissances et vitesses afin d'illustrer les résultats possibles lorsque l'on modifie ces deux paramètres.  Le matériel que l'on a choisi n'avait pas encore été testé donc nous avons choisi des paramètres réalistes avec l'aide de Julien.   

Comme nous étions pressés par le temps nous avons simplement dessiné sur Inkscape un rond (à graver) dans un rectangle (à découper). Comme cette découpeuse est un peu capricieuse c'était le plus simple pour faire nos premiers tests. Pour nos découpes nous devons apporter nos propres matériaux! Pour ce premier cours nous ne le savions pas encore mais, heureusement, il restait des bouts de récup dans la salle et nous avons pu faire notre essai sur un bout de plastique restant. 

![test](images/module 4/test.jpg)

## 4.3 Création personnelle

Comme je n'avais pas vraiment créé quelque chose de satisfaisant lors de la séance de la semaine dernière j'ai decidé de refaire une création seule pendant les séances en autonomie. 

Mon chat étant relativement âgée, elle a de l'arthrose, et donc des douleurs lorsqu'elle doit se baisser pour manger ses croquettes. J'ai donc décidé de créer un "réhausseur à croquettes"!

Pour cela je me suis inspirée du site [boxes.py](https://www.festi.info/boxes.py/) pour trouver une jolie boite dans laquelle je pourrais ensuite faire un trou pour mettre son petit bol à croquettes.  Ce qui est très pratique avec ce site c'est que certains paramètres peuvent être directement changés sur le site et on peut ensuite finaliser le travail sur InkScape si besoin.  J'ai choisi la [boîte régulière] (https://www.festi.info/boxes.py/RegularBox?language=fr) car j'aimais bien sa forme et il y avait déjà une possibilité de faire un trou sur le site en lui-même, ce qui faciliterait mon travail.

![boîte choisie](./images/module 4/boitechoisie.webp)

J'ai modifié certains paramètres pour avoir la hauteur que je voulais, j'ai découpé un trou sur un des couvercles de la boîte et j'ai modifié l'épaisseur de mon matériel. Pour ma boîte j'ai été acheter du multiplex d'épaisseur ```3,6 mm``` par peur de ne pas avoir assez de matériel sur une chute restante du FabLab.  

![paramètres](images/module 4/paramètres.png) 

Le plus difficile aura été que sur les paramètres du site ne permettaient pas de choisir la taille du trou de la boîte. J'ai donc dû faire mes dernières modifications sur InkScape. Pour cela j'ai fait des tests de type "essai-erreur" en modifiant le paramètre ```rayon``` et en mesurant le diamètre que ce paramètre donnait avec l'outil ```mesure``` sur InkScape.  J'ai pris le bol avec moi lors de la réalisation de la boîte et j'ai mesuré un diamètre d'environ ```11 cm```. Après 7 "essai-erreur" c'est avec le paramètre rayon de ```78``` que j'obtiens environ le diamètre de mon bol.  Le dernier paramètre que j'ai dû modifier a été l'épaisseur des traits pour la découpeuse laser. En effet, pour l'Epilog il est impératif que les traits qui servent de guide pour la découpe soit inférieurs à ```2,5 mm```.

Une fois tous les paramètres nécessaires modifiés, j'ai mis mon fichier sur une clé USB pour ensuite l'ouvrir sur l'ordinateur de l'Epilog. J'ai décidé d'utiliser cette découpeuse car, lors de la présentation de la semaine passée, je l'ai trouvé plus facile à utiliser et cela me permettait de savoir utiliser 2/3 découpeuses du FabLab.  Comme c'était la première fois que j'utilisais l'Epilog j'ai bien revu le [tutorial] (https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md) de cette machine et j'ai lancé la découpe avec l'aide de Julien.

Le FabLab avait déjà fait des tests de puissance et de vitesse pour le multiplex de 3,6 mm, j'ai pu donc choisir ces paramètres en me basant sur les résultats des tests. 

J'ai choisi:

* Puissance de ```70```
* Vitesse de ```10```

Avec ces paramètres j'ai dû passer 2 fois dessus pour avoir une bonne découpe. J'ai eu un petit problème lors de la découpe, comme mon matériel n'était pas tout à fait plat mais un peu gondolé, Julien m'a dit de mettre des poids pour essayer de le rendre aussi plat que possible. Mais malheureusement ce n'était pas assez  aplati car quand j'ai enlevé ma plaque de la machine j'ai remarqué que les pièces découpées ne s'enlevaient pas aussi facilement. J'ai dû donc repasser sur les morceaux mal découpés avec un cutter, puis poncer les bords. Je ne pouvais pas remettre ma planche dans la découpeuse car elle n'aurait pas recoupé exactement au même endroit.

![découpe](./images/module 4/découpe.jpg)

Après tout cela j'ai pu assembler les différentes pièces et mettre le bol dans la structure et voilà mon "réhausseur à croquettes"!  On peut voir les légères imperfections dû à la mauvaise découpe de certains morceaux.

![résultat1](./images/module 4/résultat1.jpg)

![résultat2](./images/module 4/résultat2.jpg)

![avecmonchat](images/module 4/avecmonchat.jpg)
