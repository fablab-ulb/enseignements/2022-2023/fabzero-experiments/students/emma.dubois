# 3. Impression 3D

Cette semaine nous avons eu une introduction aux imprimantes 3D et nous avons pu imprimer la pièce que nous avions modélisée lors du **module 2** de la semaine passée.

N'ayant jamais utilisé d'imprimante 3D j'étais très contente d'apprendre les bases d'utilisation de cet outil pour mes projets personnels futurs. 

## 3.1 Slicer 
Nous travaillons avec des imprimantes _Prusa_ au FabLab et donc j'ai dû installer le logiciel [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903) pour pouvoir imprimer ma pièce.  Un slicer est un logiel de découpage en tranches; il est indispensable pour l'impression 3D, ce type de logiel permet aussi de convertir les modélisations réalisées grâce aux logiciels vu au module 2 (FreeCAD et OpenSCAD) en instructions pour l'imprimante. 

Lors de l'installation du logiciel, j'ai choisi le mode _expert_ pour avoir toutes les fonctionalités du logiciel et j'ai ensuite selectionné le modèle de l'imprimante Prusa qui est à notre disposition au FabLab.

![type imprimante](./images/module 3/typeimprimante.webp)

Pour la suite des instuctions j'ai suivi le [tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) sur la page du module.

### 3.1.1 FlexLink sur le logiciel
Pour transférer mon code sur le slicer il m'a fallu transformer le fichier de ```.scad``` en ```.stl``` sinon le logiciel d'impression ne reconnaissait pas le fichier.  J'ai eu du mal à transformer mon fichier mais j'ai finalement trouvé mon problème avec l'aide de ma mère. J'avais oublié de _Render_ mon code avant de pouvoir sauvgarder mon fichier en ```.stl```.  _Render_ permet de transformer le code écrit en un fichier avec lequel on peut ensuite interagir.

J'ai ensuite uploadé mon fichier afin de visualiser mon FlexLink sur le slicer.

Voici ce que cela donne :

![FlexLinkSlicer](./images/module 3/FlexLinkSlicer.png)
On a maintenant accès à plusieurs paramètres et carctéristiques de l'impression. 

## 3.2 Impression 
Avant l'impression on peut regarder comment l'imprimante va réaliser le modèle. Cela peut nous permettre de voir si nous avons besoin d'un support afin d'éviter que la pièce ne s'écroule ou de visualiser si certaines parties auront des problèmes à s'imprimer. 

![slicer](./images/module 3/Slicer.png)

Sur le logiciel du slicer on peut aussi voir les différentes parties de la pièce **(en haut à gauche de l'écran)** ce qui nous donne la quantité de matériel dont nous aurons besoin ou encore le temps necessaire à l'impression.  Avec le curseur on peut également voir les différentes couches de remplissage et s'assurer que chaque partie sera bien réalisée (qu'il n'existe pas de malformation que l'on ne verrait pas de l'exterieur).

Après avoir tout vérifié, j'ai exporté mon **G-Code** pour le mettre sur une **carte SD** qui a ensuite été insérée dans l'imprimante 3D. Avant de débuter l'impression j'ai verifié qu'il y avait assez de PLA (matériel utilisé pour l'impression) et j'ai nettoyé la plaque avec de l'alcool puis j'ai débuté l'impression.   Ma pièce a mis 9 minutes à être réalisée sans aucun souci.

**_Voici le résultat final:_**

![flexlink3D](./images/module 3/flexlink3D.jpg)

J'ai observé qu'il y avait de petits filaments de PLA dans les trois de ma pièce, je pense que cela est dû à une petite erreur dans mon code, je vais me renseigner au prochain cours (j'ai demandé à d'autres étudiant qui m'on dit que c'était sûrement la machine cat ils avaient eu le même problème).

![petitsfiles](./images/module 3/petitsfiles.jpg)

La taille des trous est légèrement trop petite pour les legos mais mon FlexLink arrive quand même (avec un peu de résistance) à s'attacher.

## 3.3 Kit
Pour le kit, Suzanne, Eliott, Martin et moi-même, n'avions pas vraiment d'idée précise lorsque nous avons crée nos pièces respectives. Ceci à ajouter une difficulté supplémentaire après l'impression pour trouver un mécanisme compliant. Nous avions 3 pièces qui faisaient plus ou moins le même mouvement de "catapule", nous sommes donc partis sur l'idée de créer, tout simplement, une giga-catapulte. Avec les 3 pièces et le lego de Suzanne, nous pouvons ainsi faire un mouvement plus poussé pour catapulter. C'est [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/) qui nous a construit cette très belle structure avec un siège pour un petit personnage Lego qui peut donc se faire catapulter grâce à notre mécanisme compliant.

![kit](./images/module 3/kit.jpg)

Notre mécanisme en action : [ici](https://youtube.com/shorts/pfuB6SIVjEQ?feature=share)

