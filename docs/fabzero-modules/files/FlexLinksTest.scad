/*
* File : FlexLinksTest
* Author : Emma Dubois
* Date : 08-03-2023
* Copyright (c) [2023], Emma Dubois
* licenced under the MIT Licence 
*/

// Code by Ghani Sajl form "flexlink" under MIT Licence

//parametres
$fn=120;

longueur=15.8;
largeur=7; 
hauteur=3.2;
longueur_branche=50;

hole_r=2.5; 
dist_hole=8;

eps=-5;// pour couper en dessous

difference(){
    hull(){ // permet de fusionner 
    translate([0,largeur/2,0])cylinder(h=hauteur,r=largeur/2); // translate pour bouger l'élément de base le long d'un vecteur 
        // permet d'avoir le bout rond a l'extrémité
    //cube([longueur,largeur,hauteur]); // comprends pas le but

    translate([longueur,largeur/2,0])cylinder(h=hauteur,r=largeur/2);} // permet d'avoir une des deux formes

    translate([longueur/4,largeur/2,0])cylinder(h=hauteur*2,r=hole_r); // crée le premier trou

    translate([longueur/4,largeur/2,eps])cylinder(h=hauteur*2,r=hole_r); // pour couper en dessous du premier trou

    translate([(longueur/4+dist_hole),largeur/2,0])cylinder(h=hauteur*2,r=hole_r);// crée le premier trou

    translate([(longueur/4+dist_hole),largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);}// pour couper en dessous du premier trou

//Création de l'extrémité avec les trous



translate([longueur,largeur/2,0])cube([longueur_branche, largeur/8,hauteur]);//création de la tige de connexion

translate([longueur+longueur_branche,0,0])difference(){
    hull(){
    translate([0,largeur/2,0])cylinder(h=hauteur,r=largeur/2); // permet d'avoir la deuxième forme
    //cube([longueur, largeur,hauteur]); // idem que plus haut

    translate([longueur,largeur/2,0])cylinder(h=hauteur,r=largeur/2);}
    
    translate([longueur/4,largeur/2,0])cylinder(h=hauteur*2,r=hole_r);

translate([longueur/4,largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);

    translate([(longueur/4+dist_hole),largeur/2,0])cylinder(h=hauteur*2,r=hole_r);

    translate([(longueur/4+dist_hole),largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);}
//création de la deuxième extrémité
