# 1. Gestion de projet et documentation

Ce premier module nous permet d'acquérir les bases essentielles pour suivre le cours FabZero. Le module nous apprend à utiliser Git, Markdown et créer son propre site-web.

## 1.1 BASH, terminal et Git BASH

Avec mes études en Bioingénieur, je n'ai pas du tout les bases en informatique, ce premier module a donc été très compliqué pour moi.
Pour commencer, il faut avoir BASH sur son ordinateur ce qui était déjà mon cas. J'ai ouvert le terminal et j'ai suivi le [tutorial - CLI](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/command-line.md).
J'ai encore un peu de mal à comprendre la différence entre les différents terminaux. Mes plus gros problèmes sont apparus quand j'écrivais certaines lignes de commande dans le terminal Windows et pas BASH.
Ce tutorial est assez intuitif et m'a bien préparé pour la suite.

J'ai ensuite installé Git Bash qui sera utile pour la modification de mon site web. Git nous permet de travailler sur notre site 'à distance' et de faire toutes les modifications que l'on veut sans devoir travailler proprement du premier coup et que toutes les modifications soit directement ajoutées sur le serveur. On peut ainsi décider quand on veut ajouter nos dernières modifications sur notre site _publique_.

## 1.2 Clé SSH
J'ai eu beaucoup de mal pour cette partie. J'ai réussi à m'en sortir avec l'aide des autres étudiants du cours. L'utilité de cette clé est de pouvoir avoir une connexion sécurisée entre mon ordinateur et le serveur sur lequel on peut trouver mon site (ici Git).

Une clé SSH est constituée de 2 clés reliées entre elles. Une clé _privée_ sur mon ordinateur et une clé _publique_ sur le serveur.  Pour établir la clé SSH j'ai suivi [ce guide](https://docs.gitlab.com/ee/user/ssh.html).

1. On commence par génerer les clés. J'ai utilisé la clé **ED25519**

![image de la clé](./images/module 1/image de la clé.png)

2. Après avoir générer mes deux clés j'ai dû les lier à mon GitLab en allant sur mon compte dans _User Settings_ puis _SSH Keys_ et copier le lien créé dans mon terminal, sur mon compte GitLab.

3. Maintenant que mes clés publique et privée sont maintenant créés et liées, je dois cloner ma clé SSH. J'ai encore du mal à comprendre pourquoi on doit faire cela et j'ai dû avoir pas mal d'aide pour cette partie-là. 

![image du clonage ici](./images/module 1/image du clonage ici.png)

Un problème que j'ai rencontré dans cette partie est que j'avais du mal à me reperer dans mes différents répertoires donc certaines lignes de commande ne fonctionnaient pas. J'ai du donc naviguer dans mes dossiers via mon terminal afin de me resituer.

**Pour cela j'ai utilisé la fonction cd sur mon terminal**

## 1.3 Documentation de mon travail 

Pour cette partie j'ai suivi le [tutorial - Documentation and version control](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md).

### 1.3.1 Markdown
Markdown est le langague que l'on va utiliser pour réaliser notre site.
Pour commencer, il faut installer _VisualStudio_ (text editor) qui sert d'interface utilisateur grâce à laquelle nous allons rédiger notre site.

Pour apprendre à rédiger en Markdown j'ai suivi [ce tutorial](https://www.markdowntutorial.com/) interactif qui initie bien au langage.  J'ai aussi trouvé une bonne [cheat-sheet](https://www.markdownguide.org/extended-syntax/#fenced-code-blocks) au cas où j'aurais de petits trous de mémoire.

**Attention** quand on rédige en Markdown notre fichier s'enregistre sous forme **.md** mais pour le site-web il faut que le fichier soit en **.html**. Il faut donc convertir le fichier, pour cela on installe _Pandoc_.  Pour éviter de devoir convertir à chaque fois le fichier .md en .html j'ai utilisé une fonction que j'ai enregistrée sur mon GitLab. Cette fonction se trouve sur [GitHub](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown).

![fonction](./images/module 1/fonction.png)

## 1.4 MkDocs
Cet outil permet de genérer des sites web utilisés pour de la documentation de projet. C'est avec cet outil que je peux changer le thème de mon site, le nom attribué,... Pour installer cet outil j'ai suivi le [tutorial](https://www.mkdocs.org/getting-started/). 

Tout d'abord j'ai du [installer](https://www.mkdocs.org/user-guide/installation/) pip qui est une "package manager" Python qui permettra ensuite d'installer MkDocs. Les différentes installations se font dans le terminal Windows sur mon ordinateur. 

Pip et MkDocs installés, je peux maintenant modifier le fichier se trouvant déjà sur mon GitLab avec mes données ainsi que changer le thème.

## 1.5 Photos et vidéos
Pour tout ce qui est de l'ajout de photos et vidéos sur mon site, j'utilise l'outil [imageshack](https://imageshack.com/setup/promo). Cet outil en ligne est très facile à utiliser et ne nécessite que de se créer un compte. On peut ensuite copier l'URL de l'image pour l'utiliser sur son site. On peut aussi changer la résolution de l'image (le nombre de pixels) ainsi que redimensionner les images. Sur un siteweb comme celui-ci, des images à haute résolution n'ont pas vraiment d'utilité et surtout prennent énormément de place. C'est pour cela qu'il est important de bien réduire le nombre de pixels des images qu'on utilise. Toutes mes images sont en 600x600.

 **Attention** cet outil a une période d'essai gratuite de seulement 30 jours.

Après un retour de Denis, j'ai déplacé toutes mes images pour les mettre auprès de mes fichiers textes sur GitLab afin d'en assurer la pérenité au cas où le serveur externe utilisé coupe mon accès à mes images.

## 1.6 Mettre à jour son site
Le serveur est mis à jour 2 fois par semaine, mais tout d'abord nous devons synchroniser notre fichier avec le serveur. Pour cela il faut faire un _pull_ pour télécharger les nouvelles modifications du fichier puis un _push_ pour envoyer les modifications au serveur.  Chaque modification doit être nommée.  Pour ajouter des fichiers ou des photos sur Git, il suffit de les ajouter dans les dossiers désignés pour ça dans le template déjà fait.

![photo push](./images/module 1/photo push.png)

**Attention** il faut bien mettre tous les fichiers au bon endroit et dans le même dossier. Tout doit se trouver dans le dossier qui contient le template de notre site. Pour moi ce dossier se trouve: _C\Users\Hp\emma.dubois_


## 1.7 Utilisation de la documentation
Pour bien gérer ma documentation, j'essaye de prendre un maximum de photos et de tout écrire le plus vite possible. J'arrive rarement à tout écrire le soir même mais je finis toujours la majeure partie de ma documentation le week-end après le cours.  Je ne fais pas souvent de _push_ car je n'aime pas publier mon travail alors qu'il n'est pas bien fini. J'écris ma documentation dans mes notes avant de tout mettre sur Visual Studio et ensuite faire mes _push_ sur Git.  J'essaye d'implémenter principalement la méthode "spirale" pour ma documentation car il y a toujours quelques chose de plus à rajouter dans chaque module. Je fais donc la majeure partie de la documentation chaque semaine et je me mets comme limite les vacances de Pâques pour finaliser mon travail.



