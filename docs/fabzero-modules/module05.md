# 5. Dynamique de groupe et projet final

Dans cette section je regroupe la documentation de plusieurs cours axé sur le travail en groupe et la recherche de solutions face aux problématiques.

## 5.1 Exercice de résolution face à une problématique

**Pour cet exercice j'ai travaillé avec [Martin Gilles](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin) nous avons fait toutes nos recherches ensemble. Je me suis ensuite chargée d'organiser nos idées et de les mettre en forme. Martin s'est chargé de la documentation de notre travail.**

Lors de notre projet final, et plus généralement au cours de notre vie, nous serons confrontés à différentes problématiques qu'il nous faudra apprendre à gérer et par la suite à résoudre. Une manière de faire face à ces problématiques se trouve dans les concepts d'arbre à problème **et** d'arbre à solution**. Ces concepts sont faciles à prendre en main et donnent des résultats encourageants. Une courte vidéo explicative est disponible [ici](https://youtu.be/9KIlK61RInY). 

### 5.1.1. Identifier le problème principal (tronc)

Après avoir parcouru les [17 Sustainable Development Goals](https://sdgs.un.org/) listés par les nations unies, nous avons décidé de nous intéresser à la problématique de l'accès à l'eau potable en Afrique. En effet, dans le cadre du cours de pollution du milieu physique, une slide nous avait particulièrement marqué: 

![image eau](./images/module 5/imageeau.webp)

**Répartition en % des ressources en eau potable et des populations par continents.**

Cette slide démontre les problèmes liés à l'accès à l'eau potable dans certaines parties du globe. Nous pouvons observer qu'en Europe, malgré une disponibilité en eau plus faible proportionnellement à l'Afrique, la population ne souffre en général pas ou peu du manque d'eau. Un problème qui est au contraire très présent en Afrique et nous allons essayer de comprendre pourquoi. 

Afin de rendre notre problématique plus concrète et nos actions plus réalisables, nous avons essayé de cibler un problème précis et nous nous sommes renseignés sur **l'accès à l'eau potable au Sénégal**.

```
L’accès à l’eau potable demeure un problème au Sénégal, pour certaines populations, en particulier celles des quartiers pauvres ainsi que des zones rurales. De nombreuses localités ne disposent pas encore d’eau courante à domicile et s’approvisionnent à partir des puits et bornes fontaines publiques. Jusqu’à fin 2009, un peu plus de 26% de la population rurale s’approvisionnaient encore à partir de sources d’eau non « potabilisée ». En 2015, ce taux se réduit à 14%.

```
* Source : [CNCD](https://www.cncd.be/eau-senegal-interets-prives-bien-public-suez)

### 5.1.2. Identifier les causes (racines)

Après une recherche bibliographique sur le sujet, nous avons identifié différentes causes ayant un impact plus ou moins équivalent. 


 * **Privatisation** de l'eau potable sous forme d'un partenariat publique-privé
 * **Manque** d'investissement dans les infrastructures 
 * Croissance démographique **exponentielle**
 * **Réchauffement** climatique, impliquant aridité, érosion et ruissellement. 

### 5.1.3. Identifier les conséquences (branches)

Les principales conséquences qui en découlent sont les suivantes : 

* **Maladie hydrique** et de santé publique 
* **Instabilité** croissante dans le pays entre les autorités et la population locale 
* Augmentation du **cout** de l'eau 
* Problèmes d'**hygiènes** (de nombreux habitants retournent à des moyens basiques d'extractions d'eau comme les puits ou l'irrigation)

### 5.1.4. L'arbre à soucis 

![arbre faché](./images/module 5/arbrefaché.webp)

### 5.1.5 **Arbre à objectifs** 
Avec l'arbre à objectif, l'idée est de transformer le problème identifié en un objectif à atteindre pour le solutionner, tout en se donnant les moyens de remplir cet objectif. 

### 5.1.6 Problème -> objectif 

Dans notre cas, nous avons considéré qu'une _amélioration des infrastructures d'accès à l'eau_ était un objectif concret et réalisable qui pourrait offrir une solution à notre problématique. 

### 5.1.7 Causes -> Activités 
* A. Sensibilisation globale

L'idée serait de conscientiser le monde (et principalement les jeunes) à notre problématique afin d'élargir le spectre d'entités capables de trouver des solutions. Pour cela, nous pourrons faire appel par exemple à [EYP (European Youth Parliament)](https://eyp.org/). Une organisation dont le but est de faire participer des jeunes venus de toute l'Europe à des séminaires de réflexions sur certaines thématiques et de trouver des solutions pratiques à celles-ci.
* B. Sensibilisation locale

A travers [Amnesty International](https://www.amnesty.org/fr/), il est possible de lancer des actions de plaidoyer visant les chefs d'état d'un certain pays pour cibler une problématique spécifique. Le but serait alors de déprivatiser l'accès à l'eau afin de transiter vers une politique publique de celle-ci.
* C. Soutien financier  

Des récoltes de fonds organisés par l'ONG [Eau-vives](https://www.cariassociation.org/Organismes/Eau-Vive-Senegal) pourraient être relayées à plus grande échelle afin de créer un budget conséquent capable de soutenir la construction de nouvelles infrastructures. 
* D. solutions pratiques  

Une façon de trouver des solutions pratiques serait de faire appel au réseau des différents Fablab. Comme lors des inondations en Inde, il serait alors possible de mobiliser plusieurs Fablab dans le monde afin de créer et d'établir des solutions rapides et facilement applicables par les locaux.

### 5.1.8 Conséquences -> Résultats 
* **Déprivatisation** de l'eau potable
* Amélioration de l'**hygiène** de vie des habitants 
* Augmentation des offres d'**emplois** dans le pays cible 
* Diminution du **coût** de l'eau

### 5.1.9 **L'arbre à solutions** 

![abre content](./images/module 5/arbrecontent.webp)



## 5.2 Formation des groupes et début de brainstorming 

Ce cours a été très important pour notre projet final. Il nous a permis de former nos groupes ainsi que de trouver notre thématique.  Une chose importante apprise ce jour-là a été la différence entre _thématique_, _problématique_ et _solution_ et surtout l'importance de bien établir ces points dans l'ordre afin d'avoir un projet final concret et bien établi.

### 5.2.1 Formation des groupes

Pour ce cours nous avons dû apporter un objet qui représentait au mieux une _problématique_ qui nous tenait à coeur.  J'ai donc pris un coquillage pour illustrer l'impact négatif de l'humain sur les océans que ce soit le changement climatique en général ou la surpèche, les marrées noires,... Cette problématique est quelque chose qui me tient particulièrement à coeur car nos océans représentent plus de 70% de la surface de la Terre, leur santé et leur équilibre est donc extrêmement important si nous voulons minimiser l'impact du réchauffement climatique.

Arrivés au cours nous avons d'abord déposé nos objets au sol pour permettre à tout le monde de les observer, avant de les reprendre pour se mettre en cercle et expliquer à tout le monde la problématique pour laquelle nous avions choisi cet objet. 

Après que tout le monde se soit exprimé, nous nous sommes réunis en groupe de 4-5 en fonction de notre problématique.  Je me suis retrouvée avec:

* [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski)
* [Chiara](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro)
* [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau)
* [Alicia](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Alicia.gimza)

Notre groupe s'est formé autour des océans et sur la problématique de la déstruction par l'Homme.

### 5.2.2 Brainstorming

Une fois nos groupes formés, nous avons commencé le brainstorming pour établir clairement notre _thématique_ et notre _problématique_. Le brainstorming s'est fait en plusieurs exercices nous permettant de mettre nos idées en commun ainsi que de les approfondir. Cette séance a eu pour but de nous amener à établir les points importants pour la suite de notre projet. 

1. Mots en commun:

Pour commencer nous avons écrit sur une feuille tous les mots ou concepts qui se rapprochent le plus des 5 problématiques de notre groupe. Nous avons réussi à en trouver 26 en 3 minutes! 

2. Choix de la thématique:

Pour nous, le choix de la thématique à été assez rapide. Nous nous sommes assez vite retrouvés sur la même page par rapport à notre problématique commune et nous n'avons donc pas ressenti le besoin de faire l'exercice de "mise en accord" sur la thématique. 

Nous allons donc nous concentrer sur la thématique de **L'impact négatif de l'Homme sur les océans**

3. Idées de problématiques:

Pour cette partie du brainstorming, nous avons du établir plusieurs problématiques que nous allons ensuite  développer plus tard dans l'année.  Pour cela nous avons fait un exercice qui consiste à citer des problématiques au reste du groupe en commençant notre phrase par _"Moi à ta place"_ cela nous permet d'ouvrir la discussion et peut-être de découvrir de nouvelles idées auquelles nous n'avions peut-être pas pensé avant.

Après cet exercice nous avons fait appel à de l'aide extérieure. Chiara est allée dans deux groupes différents et leur a parlé de notre thématique principale, les membres des groupes ont ensuite proposé des idées de problématiques que l'on pourrait aborder lors de notre projet final. 

Avec toutes ces idées, nous avons fait un grand tri pour ne garder que les idées qui nous parlaient le plus. 

![idées](./images/module 5/idées.jpg)

## 5.3 Outils pour la dynamique de groupe

Lors de ce cours nous avons abordé les différentes manières de gérer la dynamique de groupe. Comme nous nous connaissons peu, ce cours a été une bonne opportunité pour "poser les bases" de notre groupe et pour en apprendre un peu plus sur le fonctionnement des membres du groupe.

Nous avons utilisé plusieurs outils différents, je vais documenter les trois qui m'ont le plus parlé:

### 5.3.1 L'art de décider

Des décisions peuvent être prises soit seul soit en groupe mais il est aussi possible qu'aucune décision ne soit prise. Effectivement, ne pas prendre de décision est une décision en elle-même, parfois nous n'avons pas toutes les informations pour pouvoir se décider ou alors il est trop difficile de se mettre d'accord et donc ne pas prendre de décision à ce moment-là est préférable.

Savoir se répartir la prise de décision est très important pour gader une bonne dynaique de groupe. Ttoutes les décisions doivent pas être prise en groupe car on perdrait plus de temps à se mettre d'accord sur un sujet qui ne demande pas forcément que tout le monde le soit. Si on prend l'example du choix du lieu de réunion, il n'est pas nécessaire de regrouper tous les membres d'une équipe pour décider. Il faut donc savoir déléguer des tâches moins importantes qui ne vont pas impacter l'avancement du projet.

Il faut quand même garder les prises de décisions importante en groupe. Ces décisisons sont souvent celles qui vont impacter directement le projet, que ça soit par rapport à la problématique,  les méthodes utilisées ou  la réalisation en elle même.  Pour la prise de décisions en groupe, il est bien de connaitre quelques techniques pour prendre ces décisions rapidement et en prenant l'avis de tout le monde en compte. Ce cours nous a appris les méthodes suivantes: le consensus, la température, le vote pondéré, le tirage au sort, sans objections et le jugement majoritaire.

### 5.3.2 Température 

Cette méthode de prise de décisions permet de déterminer à quel point chaque membre du groupe aime l'idée qui est discutée lors d'une réunion. Cette technique est rapide et permet de visualiser où chacun se situe.  Pour commencer tout le monde met son bras à l'horizontal et au top départ chacun place son bras plus haut pour dire qu'ils sont partants pour l'idée, laisse leur bras où il est pour dire qu'ils n'ont pas d'avis spécifique et sinon baisse leur bas pour dire qu'ils s'opposent à l'idée.  Si tout le monde a soit le bras levé ou neutre, l'équipe peut passer à un autre sujet et la décision est prise sans devoir débattre et perdre du temps vu que tout le monde est d'accord. Si certains membres du groupe ont le bras baissé, alors les discussions peuvent commencer et on peut essayer d'arriver à un accord.

Cette méthode a déjà été utilisé dans notre groupe.

### 5.3.3 Plan de réunion

Cet outil est pour moi essentiel pour le bon déroulement d'un projet en équipe. Il permet de garder les choses au clair et apporte un  maximum de structure,  ce qui aura un impact direct sur la dynamique du groupe.  Faire un plan de réunion permet aussi d'organiser le travail à faire entre les différentes réunions et donc permet de garder une vue claire sur les objectifs à atteindre ainsi que de  garder un oeil sur la limite de temps qui est très important dans le cadre de ce projet. 

Une idée de plan de réunion pourrait être: 

* Météo: faire un tour de table pour permettre à tout le monde de donner son état d'esprit et son niveau d'énérgie. Cela permet de voir qui sera plus réactif ou alors plus en retrait lors de la réunion, ce qui est très important afin de garder une bonne dynaique de groupe.

* ODJ: l'ordre du jour est une liste de toutes les choses qui doivent être abordées lors de la réunion. Souvent cet ordre du jour consisite à lister les tâches qui ont été réalisé la semaine d'avant. Avant la réunion, il est toujours bien de revoir cet ordre du jour et possiblement  d'y rajouter certains sujets supplémentaires qui devront être abordés.

* La réunion en elle même

* La répartition des tâches et agenda: organisation du travail à faire pour la prochaine réunion et possiblement la mise au point d'activités ponctuelles importantes comme un test du prototype,...




