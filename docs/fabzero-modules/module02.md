# 2. Conception Assistée par Ordinateur (CAO)

Dans ce module nous avons appris les bases de la CAD et nous avons créé un objet qui sera ensuite imprimé en 3D dans le module suivant.
Les CAD sont des logiciels qui permettent de modéliser et visualiser des modèles créés par des formes géométriques.

Pour commencer, nous avons dû installer plusieurs logiciels dont FreeSCAD et OpenSCAD.

## 2.1 Inkscape
Inkscape est un outil de dessin vectoriel, c'est-à-dire de la modélisation 2D. C'est un outil assez facile à prendre en main pour commencer le design graphique.  Le grand avantage des outils de dessin vectoriel est que l'image réalisée n'est pas faite de pixels mais est bien un trait uniforme. Cela donne alors une meilleure résolution à l'image. 

## 2.2 FreeCAD
FreeCAD est un logiciel open-source qui permet la modélisation paramétrique. Ce logiciel a une interface graphique qui permet de modéliser sans l’utilisation de code, ceci le rend plus accessible pour les personnes moins à l’aise avec la programmation. Ce format permet aussi de faire des modélisations de façon plus créative (sans nécessiter de connaissances mathématique ou de programmation).

Pour se familiariser avec ce logiciel, j’ai suivi les [vidéos explicatives](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/FreeCAD.md) pour modéliser un cœur. Les vidéos sont très claires et m’ont permis d’y arriver (presque) du premier coup.

Ma première modélisation:

![coeur](./images/module 2/coeur.png)

## 2.3 OpenSCAD
  OpenSCAD est un autre logiciel de modélisation paramétrique. Ce logiciel est très utilisé pour les découpeuses laser ainsi que les imprimantes 3D.  Pour l’utilisation de OpenSCAD voici la [cheat sheet](https://openscad.org/cheatsheet/) pour faciliter sa prise en main.

### 2.3.1 Thingiverse
[Thingiverse](https://www.thingiverse.com) est un site de partage de fichiers openSCAD décidés à l’impression 3D. Ce site est collaboratif et possède une licence créative libre permettant aux utilisateurs d’accéder librement aux codes pour leurs propres créations. 

## 2.4 Design FlexLink
Pour ce module nous devons créer un [FlexLink](https://www.compliantmechanisms.byu.edu/flexlinks) qui est un type de compliant mechanism: c’est-à-dire un objet qui rempli une fonction mécanique basée sur l’élasticité du matériel.

Pour ma création j’ai décidé de réaliser le **Fixed Fixed Beam**

![Fixed-Fixed Beam](./images/module 2/fixed-fixed beam.webp)

N’ayant pas de très bonnes bases en programmation et étant pressée par le temps, je me suis fortement inspirée du travail de [Ghani Sajl](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/FabZero-Modules/module02/) et j'ai utilisé la [cheat sheet](https://openscad.org/cheatsheet/).

### 2.4.1 Code

```
/*
* File : FlexLinksTest
* Author : Emma Dubois
* Date : 08-03-2023
* Copyright (c) [2023], Emma Dubois
* licenced under the MIT Licence 
*/

// Code by Ghani Sajl form "flexlink" under MIT Licence

//parametres
$fn=120;

longueur=15.8;
largeur=7; 
hauteur=3.2;
longueur_branche=50;

hole_r=2.5; 
dist_hole=8;

eps=-5;// pour couper en dessous

difference(){
    hull(){ // permet de fusionner 
    translate([0,largeur/2,0])cylinder(h=hauteur,r=largeur/2); // translate pour bouger l'élément de base le long d'un vecteur 
        // permet d'avoir le bout rond a l'extrémité 

    translate([longueur,largeur/2,0])cylinder(h=hauteur,r=largeur/2);} // permet d'avoir une des deux formes

    translate([longueur/4,largeur/2,0])cylinder(h=hauteur*2,r=hole_r); // crée le premier trou

    translate([longueur/4,largeur/2,eps])cylinder(h=hauteur*2,r=hole_r); // pour couper en dessous du premier trou

    translate([(longueur/4+dist_hole),largeur/2,0])cylinder(h=hauteur*2,r=hole_r);// crée le premier trou

    translate([(longueur/4+dist_hole),largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);}// pour couper en dessous du premier trou

//Création de l'extrémité avec les trous



translate([longueur,largeur/2,0])cube([longueur_branche, largeur/8,hauteur]);//création de la tige de connexion

translate([longueur+longueur_branche,0,0])difference(){
    hull(){
    translate([0,largeur/2,0])cylinder(h=hauteur,r=largeur/2); // permet d'avoir la deuxième forme

    translate([longueur,largeur/2,0])cylinder(h=hauteur,r=largeur/2);}
    
    translate([longueur/4,largeur/2,0])cylinder(h=hauteur*2,r=hole_r);

translate([longueur/4,largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);

    translate([(longueur/4+dist_hole),largeur/2,0])cylinder(h=hauteur*2,r=hole_r);

    translate([(longueur/4+dist_hole),largeur/2,eps])cylinder(h=hauteur*2,r=hole_r);}
//création de la deuxième extrémité
```

**_Voici ce que cela donne sur OpenSCAD_**:

![screenshotopenscad](./images/module 2/screenshotopenscad.png)


## 2.5 Licence Creative Commons
Cette licence n'empêche cependant pas que le travail puisse circuler librement mais la licence assure le respect de la propriété intellectuelle de l'auteur. La licence choisie par l'auteur peut être plus ou moins ouverte, laissant au public plus ou moins de liberté pour l'utilisation ou la modification du travail initial. Par exemple, si l'auteur ne veut pas que son travail soit utilisé à des fins lucratifs, il pourra y attacher une Licence CC plus restrictive.

Les différentes licences sont regroupées ici (merci [Wikipédia](https://fr.wikipedia.org/wiki/Licence_Creative_Commons)):

![cc](./images/module 2/cc.png)

